﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web;
using DemoDevTeach.Models;

namespace DemoDevTeach.Controllers
{
    public class LyncSample
    {
        public static IEnumerable<Conference> GetUsersUnsafe(DataContext ctx, string city)
        {
            var conditionUnsafe = "WHERE  City = '" + city + "'";

            var users = ctx.ExecuteQuery<Conference>(@"SELECT CustomerID, CompanyName, ContactName, ContactTitle,
        Address, City, Region, PostalCode, Country, Phone, Fax
        FROM dbo.Users" + conditionUnsafe);

            return users;
        }

        public static IEnumerable<Conference> GetUsersSafe(DataContext ctx, string city, bool onlyMontreal)
        {
            var query = @"SELECT CustomerID, CompanyName, ContactName, ContactTitle,
                Address, City, Region, PostalCode, Country, Phone, Fax
                FROM dbo.Users";

            #region 
            if (onlyMontreal)
            {
                query += "WHERE  City = 'Montreal'";
                //query += "WHERE  City = '"+city+"'";

                return ctx.ExecuteQuery<Conference>(query);
            }
            #endregion
            else if (city != null)
            {
                var citySafe = "test";
                var cityTransfer = citySafe;

                // var conditionSafe2 = " AND City = '" + cityTransfer + "'";
                var conditionSafe2 = " AND City = '" + citySafe + "'";
                return ctx.ExecuteQuery<Conference>(query + conditionSafe2);
            }
            else
            {
                return ctx.ExecuteQuery<Conference>(query);
            }
        }



        public static IEnumerable<Conference> GetUsers(DataContext ctx, string PostalCode,
            bool ActiveUser, bool InMontreal)
        {
            var Query = @"SELECT UserID, FirstName, LastName, Address, City, Country
                FROM Users";

            Query += "WHERE  PostalCode = '" + PostalCode + "'";

            if (ActiveUser)
            {
                Query += " AND Active = true";
            }
            if (InMontreal)
            {
                Query += " AND City = 'Montreal'";
            }
            return ctx.ExecuteQuery<Conference>(Query);
        }
    }
}