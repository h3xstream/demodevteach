﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoDevTeach.Controllers
{
    public class CookieSample
    {
        public HttpCookie createCookie()
        {
            var cookie = new HttpCookie("userLang");
            cookie.Path = "/test";
            cookie.Value = "fr";

            return cookie;
        }
    }
}