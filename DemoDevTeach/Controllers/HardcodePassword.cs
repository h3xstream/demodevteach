﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace DemoDevTeach.Controllers
{
    public class HardcodePassword
    {
        public WebResponse test(string input)
        {
            string password = "123456";

            WebRequest r = WebRequest.Create("http://internal.company.com/api/document/1");
            r.Credentials = new NetworkCredential("adminservice", password);

            return r.GetResponse();
        }

        public void initConfiguration(string input)
        {
            var service = new Service();
            service.Password = "1234";
        }
    }
}